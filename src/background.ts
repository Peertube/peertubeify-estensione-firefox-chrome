/* This file is part of PeerTubeify.
 *
 * PeerTubeify is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * PeerTubeify is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * PeerTubeify. If not, see <https://www.gnu.org/licenses/>.
 */

import * as _ from 'lodash/fp';
import * as browser from 'webextension-polyfill';

import { MessageKind, RedirectType } from './types';
import Preferences from './preferences';
import * as InvidiousAPI from './invidious-api';
import * as PeertubeAPI from './peertube-api';
import { getPeertubeVideoURL } from './util';

/**
 * Retrieve the title of the video using Invidious API.
 */
const getTitle = async (id: string) => {
    const data = await InvidiousAPI.getVideo(id);
    return data.title;
}

/**
 * Redirect Youtube without loading it if the video is found and preferences
 * set on automatic redirection.
 */
const redirectYoutube = async (r) => {
    const prefs = await Preferences.getPreferences();

    if (prefs.redirectYoutube == RedirectType.Auto) {
        const isEmbed = _.contains('embed', r.url);

        const query = isEmbed
            ? r.url.substr(r.url.lastIndexOf('/') + 1, 11)
            : r.url.substr(r.url.indexOf('?v=') + 3, 11);

        const title = await getTitle(query);
        const video = await searchByName(title);
        const url = getPeertubeVideoURL(video, prefs, { isEmbed });

        return {
            redirectUrl: url
        };
    }
};

/**
 * Redirect to preferred PeerTube Instance when trying to watch a PeerTube
 * video.
 */
const redirectPeertube = async (r) => {
    const prefs = await Preferences.getPreferences();
    const hostname = new URL(r.url).hostname;

    if (prefs.openInOriginalInstance === false && prefs.searchInstance === hostname) {
        return {}; // Don't redirect if good instance
    }

    if (prefs.redirectPeertube == RedirectType.Auto) {
        const id = _.last(_.split('/', r.url));
        const video: any = await PeertubeAPI.getVideo(id);

        const isEmbed = _.contains('embed', r.url);

        if (prefs.openInOriginalInstance && video.account.host === hostname) {
            return {}; // Don't redirect if original instance
        }

        const url = getPeertubeVideoURL(video, prefs, { isEmbed });

        return {
            redirectUrl: url
        };
    }
};

const searchByName = query => new Promise(async (resolve, reject) => {
    PeertubeAPI.searchVideo({ search: query })
        .then(function(data) {
            if (data.total > 0) {
                const video = data.data[0]
                if (_.toLower(video.name) === _.toLower(query)) {
                    resolve(video);
                }
            }
            reject(new Error('No results.'));
        });
});

browser.runtime.onMessage.addListener(function(message) {
    switch (message.kind) {
        case MessageKind.SearchByName:
            return searchByName(message.query);
        case MessageKind.SearchByID:
            return PeertubeAPI.getVideo(message.id);
    }
});

browser.webRequest.onBeforeRequest.addListener(
    redirectYoutube,
    { urls: ['*://*.youtube.com/watch?v=*', '*://*.youtube.com/embed/*', '*://*.invidio.us/watch?v=*', '*://*.invidio.us/embed/*'] },
    ['blocking']
);

browser.webRequest.onBeforeRequest.addListener(
    redirectPeertube,
    { urls: ['*://*/videos/watch/*', '*://*/videos/embed/*'] },
    ['blocking']
);
